$(document).ready( function () {
    $('#slider_clients').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        appendArrows: $('.slider-nav'),
        prevArrow: $('.prev'),
        nextArrow: $('.next')
    });

    $('#slider_projects').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    $("#form").submit(function () {
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../send.php",
            data: form_data
        });
    });
});